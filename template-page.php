<?php

/**
Template Name: Landing Page 
 **/

get_header();

$page_name = get_post_field( 'post_name', get_post() );

?>

<script src="<?php echo get_template_directory_uri(); ?>/template-page/angular.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<div ng-app="myApp" class="content-outer-wrapper" ng-controller="templatePageController">
    <?php
    // Check and get Sidebar Class
    global $sidebar, $wpdb;
    $sidebar = get_post_meta($post->ID, 'page-option-sidebar-template', true);
    $sidebar_array = gdl_get_sidebar_size($sidebar);
    if ($sidebar == 'no-sidebar') {
        get_template_part('page', 'full');
    } else {
        get_template_part('page', 'normal');
    }
    ?>

    <?php

    $id_page_for_copy_products = get_field('id_page_for_copy_products');

    $has_short_code_form = get_field('has_short_code_form');

    $the_content_2 = get_field('the_content_2');


    $array_query =  array(
        'relation' => 'OR',
        array(
            'key'     => 'pagina_na_qual_ira_aparecer_esse_produto',
            'value'   => get_the_ID(),
            'compare' => '='
        )
    );

    if ($id_page_for_copy_products) {

        array_push($array_query, array(
            'key'     => 'pagina_na_qual_ira_aparecer_esse_produto',
            'value'   => get_field('id_page_for_copy_products'),
            'compare' => '='
        ));
    }


    query_posts(
        array(
            'post_type' => array('produtos'),
            'posts_per_page' => -1,
            'meta_query' => array(
                $array_query
            )
        )
    );



    ?>


    <div class="filtro row">

        <h2 style="text-align: center;">
            <span style="color: #f58345;" class="title1">Faça sua solicitação!</span>
        </h2>
        <div class="clear"></div>

        <?php

        $categories_names = get_field("nomes_para_o_filtro");
        $categories = get_field("categorias_para_o_filtro");

        $data_inicio_do_calendario = get_field("data_inicio_do_calendario");
        $data_fim_do_calendario = get_field("data_fim_do_calendario");

        foreach ($categories as $key => $cat) {
            //renomeia categoria 
            if (preg_split('/\r\n|[\r\n]/', $categories_names)[$key]) $cat->name = trim(strip_tags(preg_split('/\r\n|[\r\n]/', $categories_names)[$key]));

            $cat->{"categories_child"} = get_categories(
                array('parent' => $cat->term_id)
            );

        ?>
            <div class="four columns gdl-package-grid2 lista-pacotes">
                <strong> <?php echo $cat->name; ?> </strong>
                <div class="clear"></div>



                <ul class="lista_sub_cats">

                    <?php foreach ($cat->{"categories_child"} as $key2 => $sub_cat) { ?>
                        <li>
                            <label for="cat_id_<?php echo $sub_cat->term_id; ?>">
                                <input ng-model="cat_id_<?php echo $sub_cat->term_id; ?>" ng-change="changeCategory({ 'id' : <?php echo $sub_cat->term_id; ?>, 'slug' : '<?php echo $sub_cat->slug; ?>', 'column' : <?php echo $key; ?>})" id="cat_id_<?php echo $sub_cat->term_id; ?>" type="checkbox"> <?php echo $sub_cat->name; ?>
                            </label>
                        </li>

                    <?php } ?>


                </ul>

            </div>
        <?php




        }


        ?>

    </div>


    <?php get_template_part('loop-produtos'); ?>

    <div class="row form_de_contato">

        <div class="twelve">

            <?php

            if ($has_short_code_form) {

                echo do_shortcode($has_short_code_form);
            } else {
                echo do_shortcode('[contact-form-7 title="Solicitar Viagem Landing Pages"]');
            }
            ?>

        </div>

        <div class="twelve">
            <?php echo $the_content_2; ?>
        </div>

    </div>

    <div class="row" title="teste - <?php echo $has_short_code_form; ?>">
        <div class="twelve">
            <img class="template-page-img-rodape" src="<?php echo get_bloginfo('stylesheet_directory') . '/template-page/images/rodape_travel_ace.jpg'; ?>">
        </div>

    </div>


</div> <!-- content outer wrapper -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/template-page/jquery.mask.min.js"></script>



<script src="<?php echo get_template_directory_uri(); ?>/template-page/templatePageController.js"></script>


<script>
    (function($) {

        $("document").ready(function() {

            Date.prototype.addDias = function(dias) {
                this.setDate(this.getDate() + dias)
            };

            var dateFormat = "dd/mm/yy",
                dayNames = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"],
                monthNames = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                monthNamesShort = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
                dayNamesMin = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                dayNamesShort = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],

                minDate = "<?php echo $data_inicio_do_calendario; ?>",
                maxDate = "<?php echo $data_fim_do_calendario; ?>",




                from = $("#start")
                .datepicker({
                    dateFormat: dateFormat,
                    dayNamesMin: dayNamesMin,
                    monthNames: monthNames,
                    monthNamesShort: monthNamesShort,
                    dayNamesShort: dayNamesShort,
                    dayNames: dayNames,
                    defaultDate: "+1w",
                    minDate: minDate,
                    maxDate: maxDate,
                    changeMonth: true,
                    numberOfMonths: 1,
                    beforeShowDay: disableSpecifyDaysWeek
                }).on("change", function() {

                    to.datepicker("option", "minDate", getDate(this));
                    to.datepicker("option", "maxDate", dateSumDays(from[0], 7));

                    to.datepicker("setDate", dateSumDays(from[0], 7));

                    setTimeout(function() {
                        to.datepicker("show");
                    }, 500);


                }),



                to = $("#end").datepicker({
                    dateFormat: dateFormat,
                    dayNamesMin: dayNamesMin,
                    monthNames: monthNames,
                    monthNamesShort: monthNamesShort,
                    dayNamesShort: dayNamesShort,
                    dayNames: dayNames,
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    minDate: minDate,
                    maxDate: maxDate,
                    beforeShowDay: disableSpecifyDaysWeek
                }).on("change", function() {
                    //from.datepicker("option", "maxDate", getDate(this));
                });


            function dateSumDays(elem, days) {
                var data = getDate(elem);
                var sum = new Date(data);
                sum.addDias(days);
                console.log("soma", sum);
                return sum;
            }


            function getDate(element) {
                var date;
                console.log("data ", element);
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }

            function disableSpecifyDaysWeek(date) {
                var day = date.getDay();
                if (day == 1) {
                    return [false, "", "Unavailable"];
                } else if (day == 2) {
                    return [false, "", "Unavailable"];
                } else if (day == 3) {
                    return [false, "", "Unavailable"];
                } else if (day == 4) {
                    return [false, "", "Unavailable"];
                } else if (day == 5) {
                    return [false, "", "Unavailable"];
                } else if (day == 6) {
                    return [false, "", "Unavailable"];
                } else {
                    return [true];
                }
            }

        });


    })(jQuery);
</script>



<?php if( in_array( $page_name, array("promocao-club-med-livelo") ) ){ ?>
<!-- Implementação do PIXEL -->
<script>

    
    function autoPixelLivelo(UA_ID) {
        var prurl = ecom = cid = '';
        dataLayer = window.dataLayer || [];
        for (var i = 0; i < dataLayer.length; i++) {
            if (dataLayer[i].hasOwnProperty('ecommerce') &&
                dataLayer[i].ecommerce.hasOwnProperty('purchase')) {
                ecom = dataLayer[i].ecommerce.purchase;
            }
        }
        var cookies = document.cookie;
        var cookie = cookies.split(';');
        cookie.filter(function(item) {
            if (item.trim().substring(0, 4) == '_ga=') {
                cid = item.split('.');
                cid = cid[cid.length - 2] + ':' + cid[cid.length - 1];
                return;
            }
        });
        if (typeof ecom !== 'object') return false;
        for (var i = 0; i < ecom.products.length; i++) {
            var x = i + 1;
            var pid = encodeURI(ecom.products[i].id) || '';
            var pnm = encodeURI(ecom.products[i].name) || '';
            var ppr = ecom.products[i].price || '';
            var pqt = ecom.products[i].quantity || '';
            prurl += 'pr' + x + 'id=' + pid + '&';
            prurl += 'pr' + x + 'nm=' + pnm + '&';
            prurl += 'pr' + x + 'pr=' + ppr + '&';
            prurl += 'pr' + x + 'qt=' + pqt + '&';
        }
        var el = ecom.actionField.affiliation || '';
        el = 'Parceiro ' + el;
        el = encodeURI(el);
        var dl = encodeURI(window.location.href);
        var ti = ecom.actionField.id || '';
        var tr = ecom.actionField.revenue || '';
        var ts = ecom.actionField.shipping || '';
        var UA_ID = ecom.actionField.UA_ID || '';
        var z = new Date();
        var url = 'https://www.google-analytics.com/collect?';
        url += 'v=1&t=event&tid=' + UA_ID + '&';
        url += 'ec=enhanced-ecommerce&ea=purchase&pa=purchase&';
        url += 'el=' + el + '&dl=' + dl + '&ti=' + ti + '&';
        url += 'tr=' + tr + '&ts=' + ts + '&';
        url += prurl + 'cid=' + cid + '&z=' + z.getTime();
        var pixel = document.createElement('img');
        pixel.setAttribute('src', url);
        pixel.setAttribute('class', 'pixel-livelo');
        pixel.style.display = 'none';
        document.querySelector('body').appendChild(pixel);
    }

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
            vars[key] = value;
        });
        return vars;
    }

    function getUrlParam(parameter, defaultvalue) {
        var urlparameter = defaultvalue;
        if (window.location.href.indexOf(parameter) > -1) {
            urlparameter = getUrlVars()[parameter];
        }
        return urlparameter;
    }

    function setCookie(cname, cvalue, exdays, dominio) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/; domain=" + dominio + ";";
        //document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

    }

    (function() {

        try {

            if (getUrlVars()["utm_source"]  && getUrlVars()["utm_source"] != '') {

                console.log("Pixel ok 4 - ", "<?php echo $page_name; ?>" );

                var dominio = window.location.host;
                data_expiracao = 365, //days
                    origem = getUrlParam("utm_source", "");

                setCookie("utm_source", origem, data_expiracao, dominio);


                window.dataLayer = [{
                    ecommerce: {
                        purchase: {
                            actionField: {
                                UA_ID: 'UA-66602774-8',
                                affiliation: 'Sete Mares',
                                id: '',
                                revenue: '',
                                shipping: '',
                            },
                            products: [{
                                name: window.document.title
                            }]
                        }
                    }

                }];

                autoPixelLivelo();

            }


        } catch (error) {
            console.log(error);
        }
    })();
</script>

<!-- Fecha Implementação do PIXEL -->
<?php }; ?>



<?php get_footer(); ?>