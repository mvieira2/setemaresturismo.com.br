<div class="container_2" ng-init="noResult=false">
    <div class="row">
        <div class="twelve columns package-item-class package-item-class-1 mb35">
            <div class="package-item-holder">
                <div class="row">

                    <h2 style="text-align: center;">
                        <span style="color: #f58345;" class="title1">Villages</span>
                    </h2>

                    <p class="ng-cloak" style="text-align: center;" ng-show="noResult">

                        Nenhum resultado encontrado, tente outro filtro...
                    </p>

                    <?php
                    while (have_posts()) : the_post();
                        $package_name = get_field("tag_do_produto");
                        $url_image_full = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'full') : get_bloginfo('stylesheet_directory') . '/images/no-image.png';
                        $url_image_thumbnail = has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'medium') : get_bloginfo('stylesheet_directory') . '/images/no-image.png';

                    ?>


                        <div class="three columns gdl-package-grid2 lista-pacotes" title="<?php echo $package_name; ?>" ng-show="filter('<?php echo implode(", ", wp_get_post_categories(get_the_ID())); ?>')">
                            <div class="package-content-wrapper">
                                <div class="package-thumbnail-outer-wrapper">
                                    <div class="package-media-wrapper gdl-image">
                                        <a data-rel="fancybox" data-fancybox-group="products_<?php echo get_the_ID(); ?>" href="<?php echo $url_image_full; ?>">

                                            <?php
                                            echo '<img src="' . $url_image_thumbnail . '" />';
                                            ?>

                                        </a>

                                        <div class="hidden gallery-images-fancybox">

                                            <?php

                                            $images = get_field('gallery');

                                            if ($images) : ?>
                                                <ul>
                                                    <?php foreach ($images as $image) : ?>
                                                        <li>
                                                            <a data-rel="fancybox" data-fancybox-group="products_<?php echo get_the_ID(); ?>" href="<?php echo $image['url']; ?>">
                                                                <?php echo $image['sizes']['thumbnail']; ?>
                                                            </a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php endif; ?>

                                        </div>

                                        <?php

                                        if ($package_name != "") : ?>

                                            <div class="package-ribbon-wrapper">
                                                <div class="package-type normal-type"> <?php echo $package_name; ?> </div>
                                                <div class="clear">
                                                </div>
                                                <div class="package-type-gimmick">
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>

                                        <?php endif; ?>

                                    </div>
                                    <div class="package-title-wrapper">
                                        <div class="package-title-overlay">
                                        </div>
                                        <h2 class="package-title">
                                            <label>
                                                <?php the_title(); ?>
                                                <input ng-model="products_<?php echo get_the_ID(); ?>" ng-change="changeProducts('<?php echo get_the_title() . " - " . $package_name; ?>')" type="checkbox" class="checkbox">
                                            </label>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>

                </div>
            </div>

        </div>
        <div class="clear">
        </div>



    </div>



</div>