var app = angular.module('myApp', []);

app.controller('templatePageController', function ($scope) {

    $scope.categories = [];
    $scope.products = [];
    $scope.noResult = false;


    $scope.isShowErroResult = function() {
        $scope.noResult = Boolean( angular.element(document.querySelectorAll('.package-item-holder > .row .lista-pacotes')).length == angular.element(document.querySelectorAll('.package-item-holder > .row .lista-pacotes.ng-hide')).length );
    }

    $scope.changeCategory = function (cat) {

        var searchCategories = $scope.categories.filter(function (category) {
            return category.id == cat.id;
        });


        if (searchCategories.length) {

            $scope.categories = $scope.categories.filter(function (category) {
                return category.id != cat.id;
            });

        }

        else {
            $scope.categories.push(cat);
        }



    }


    $scope.changeProducts = function (prod) {

        var index = $scope.products.indexOf(prod);

        if (index > -1) {
            $scope.products.splice(index, 1);
        }

        else {
            $scope.products.push(prod);
        }

        var list_prods_string = "";

        $scope.products.map(function (p) {

            list_prods_string += p + ", ";

        });

        document.getElementById("list_products").value = list_prods_string;

  
        //$scope.isShowErroResult();


    }


    $scope.filterBKP = function (categories) {
        if ($scope.categories.length == 0) {
            return true;
        }

        categories = categories.split(", ");

        return Boolean($scope.categories.filter(function (cat) {
            return categories.indexOf(Number(cat.id).toString()) > -1;
        }).length);

        // console.log( categories)

    }




    $scope.filter = function (product_categories) {
        if ($scope.categories.length == 0) {
            return true;
        }

        product_categories = product_categories.split(", ").map(function (c) { return Number(c) });


        categories_filter_only_ids = $scope.categories.map(function (cat) {
            return Number(cat.id);
        });


        cat_column_0 = $scope.categories.filter(function (cat) {
            return cat.column == 0;
        }).map(function (cat) {
            return cat.id;
        });

        cat_other_collumns = $scope.categories.filter(function (cat) {
            return cat.column != 0;
        }).map(function (cat) {
            return cat.id;
        });

        result = false;


        if (cat_column_0.length) {


            exist_country = cat_column_0.filter(function ($p) {
                return product_categories.indexOf($p) > -1
            });

 
            list_other_categories_exist = cat_other_collumns.filter(function ($p) {
                return product_categories.indexOf($p) > -1
            })

            
            result = Boolean(exist_country.length && (cat_other_collumns.length == list_other_categories_exist.length));



        }

        else {

 
            list_other_categories_exist = cat_other_collumns.filter(function ($p) {
                return product_categories.indexOf($p) > -1
            })


            result =  Boolean(cat_other_collumns.length == list_other_categories_exist.length);

        }


        return result;





    }




});


(function ($) {


    $("document").ready(function () {


        $('.data').unmask().mask('00/00/0000');
        $('.cep').unmask().mask('00000-000');
        $('.cnpj').unmask().mask('00.000.000/0000-00');
        $('.cpf').unmask().mask('000.000.000-00');
        $('.decimal').unmask().mask('#.##0,00', { reverse: true });
        $('.moeda').unmask().mask('###0.00', { reverse: true });
        //$('.moeda').unmask().mask('#.##0,00', {reverse: true});
        $('.pontos').unmask().mask('###0.00', { reverse: true });

        //jQuery('.moeda').unmask().mask('###.###.###,00', {reverse: true});
        //jQuery('.pontos').unmask().mask('###.###.###.###.###,00');
        $('.moedaComVirgula').unmask().mask('###.###.###,00', { reverse: true });
        $('.pontosComVirgula').unmask().mask('###.###.###.###.###,00', { reverse: true });

        //$('.moedaSemVirgula').unmask().mask('#########', {reverse: true});

        $('.pontoComVirgula3CasasDecimais').unmask().mask('###.###.###.###.###,000', { reverse: true });
        $('.semvirgula').unmask().mask('##########', { reverse: true });
        $('.seisCasa').unmask().mask('###.000000', { reverse: true });
        $('.sonumeros').unmask().mask('00000000000', { reverse: true });


        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 9 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };
        $('.tel').unmask().mask(SPMaskBehavior, spOptions);


    });

})(jQuery);